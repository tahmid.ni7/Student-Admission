# Student-Admission
It is a simple insert, view, delete, update project in PHP with mysql database. It's full secured and with proper form validation. Here I create a form as student admission form to insert student information, and display the all students information in view page. I also add here delete and update option.

For protect from HTML injection I use some function here, these are-
- strip_tags()
- mysqli_real_escape_string()
- htmlentities()

It is very simple and I build it for my practise...

For better help and using the project please read the "read me.txt" file.

Thanks from Tahmid Nishat
